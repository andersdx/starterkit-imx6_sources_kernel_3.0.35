#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xc3fe87c8, "param_ops_uint" },
	{ 0x297b93fa, "no_llseek" },
	{ 0x8d14e8c9, "driver_unregister" },
	{ 0x79be06a0, "class_destroy" },
	{ 0xb809ccfa, "spi_register_driver" },
	{ 0x6bc3fbc0, "__unregister_chrdev" },
	{ 0xd44618e4, "__class_create" },
	{ 0x619efc7a, "__register_chrdev" },
	{ 0x2ec0bdb3, "spi_setup" },
	{ 0x4b0ed5c, "put_device" },
	{ 0x8ffdb206, "get_device" },
	{ 0xad998077, "complete" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x2f4ea1ac, "wait_for_completion" },
	{ 0x56bcc419, "spi_async" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xb1f56e2d, "nonseekable_open" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xdbd8e713, "device_create" },
	{ 0xd3dbfbc4, "_find_first_zero_bit_le" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x22cb404e, "device_destroy" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0x37a0cba, "kfree" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x42f006db, "mutex_lock" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

