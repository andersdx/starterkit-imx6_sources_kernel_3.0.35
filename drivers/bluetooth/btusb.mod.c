#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xb78c61e8, "param_ops_bool" },
	{ 0xe5d947e8, "usb_deregister" },
	{ 0x7ae8149a, "usb_register_driver" },
	{ 0x4cb7f01b, "usb_set_interface" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0xcf6537c9, "usb_autopm_put_interface" },
	{ 0x857b5582, "usb_autopm_get_interface" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x9ea9c0d7, "kfree_skb" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xcbe393c7, "usb_match_id" },
	{ 0x881f8377, "skb_queue_tail" },
	{ 0x1868d40c, "skb_put" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0x157c02a4, "hci_register_dev" },
	{ 0x3e4b2916, "usb_driver_claim_interface" },
	{ 0xa56e4663, "usb_ifnum_to_if" },
	{ 0x5bc145de, "hci_alloc_dev" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0x37a0cba, "kfree" },
	{ 0x6e662ba5, "hci_free_dev" },
	{ 0x1bc9d142, "usb_driver_release_interface" },
	{ 0x72c0e777, "hci_unregister_dev" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x4205ad24, "cancel_work_sync" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xe270ac5e, "usb_kill_anchored_urbs" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0xf18f2b13, "usb_scuttle_anchored_urbs" },
	{ 0x96ab4ed5, "usb_get_from_anchor" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xcc0a574, "hci_recv_fragment" },
	{ 0x7d11c268, "jiffies" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x5684c3ac, "usb_unanchor_urb" },
	{ 0x27e1a049, "printk" },
	{ 0xaf7f7c2b, "usb_free_urb" },
	{ 0x60bdc989, "usb_submit_urb" },
	{ 0xbf776f11, "usb_anchor_urb" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xa28d894f, "usb_alloc_urb" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x8949858b, "schedule_work" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("usb:v*p*d*dcE0dsc01dp01ic*isc*ip*");
MODULE_ALIAS("usb:v0A5Cp21E1d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp8213d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp8215d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp8218d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp821Bd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp821Fd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp821Ad*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v05ACp8281d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v057Cp3800d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v04BFp030Ad*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v044Ep3001d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v044Ep3002d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0BDBp1002d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0C10p0000d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0489pE042d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0A5Cp21E3d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0A5Cp21E6d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0A5Cp21E8d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0A5Cp21F3d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v413Cp8197d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0489pE033d*dc*dsc*dp*ic*isc*ip*");

MODULE_INFO(srcversion, "8EA1071E526B7DCB60AACF7");
