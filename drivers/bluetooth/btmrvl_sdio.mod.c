#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x2a6e891d, "sdio_unregister_driver" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x69c8fb01, "sdio_register_driver" },
	{ 0xdb6568a9, "release_firmware" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xd5b9b782, "request_firmware" },
	{ 0x91e626b3, "btmrvl_send_hscfg_cmd" },
	{ 0x5457bc51, "btmrvl_enable_ps" },
	{ 0xd569adf2, "btmrvl_register_hdev" },
	{ 0xe2dd707b, "btmrvl_add_card" },
	{ 0xcc7965f0, "sdio_set_block_size" },
	{ 0x45e250c1, "sdio_claim_irq" },
	{ 0x732a3c71, "sdio_enable_func" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0xfc393f91, "btmrvl_send_module_cfg_cmd" },
	{ 0x531370a5, "btmrvl_remove_card" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x71cdf5fd, "hci_resume_dev" },
	{ 0x3c5ea003, "hci_recv_frame" },
	{ 0x2b2deb1, "btmrvl_check_evtpkt" },
	{ 0x80327fb5, "btmrvl_process_event" },
	{ 0xc9bb874, "skb_pull" },
	{ 0x1868d40c, "skb_put" },
	{ 0x9ea9c0d7, "kfree_skb" },
	{ 0x77f80f2, "sdio_readsb" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0x34908c14, "print_hex_dump_bytes" },
	{ 0x37a0cba, "kfree" },
	{ 0xf1fcd7a, "sdio_writesb" },
	{ 0x9d669763, "memcpy" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xda8b5a68, "btmrvl_interrupt" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0x8c550e41, "sdio_writeb" },
	{ 0xf9a482f9, "msleep" },
	{ 0x487388af, "sdio_readb" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x211ee6cf, "sdio_release_host" },
	{ 0xd2389047, "sdio_disable_func" },
	{ 0x88b7297, "sdio_release_irq" },
	{ 0x38b3a5a4, "sdio_claim_host" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x27e1a049, "printk" },
	{ 0xd597d86e, "sdio_set_host_pm_flags" },
	{ 0xde1a6c5d, "skb_queue_purge" },
	{ 0x3e88ea9d, "hci_suspend_dev" },
	{ 0x6ed4426, "btmrvl_enable_hs" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0x249b12f0, "sdio_get_host_pm_caps" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=btmrvl";

MODULE_ALIAS("sdio:c*v02DFd9105*");
MODULE_ALIAS("sdio:c*v02DFd911A*");

MODULE_INFO(srcversion, "B236571F21DE24211C8DF1A");
