#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x951cc112, "skb_queue_head" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0x528c709d, "simple_read_from_buffer" },
	{ 0x2893e4fc, "debugfs_create_dir" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x157c02a4, "hci_register_dev" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xb4d6939, "remove_wait_queue" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0xae8e13d3, "debugfs_create_file" },
	{ 0x72c0e777, "hci_unregister_dev" },
	{ 0x9ab271af, "skb_realloc_headroom" },
	{ 0x34e55394, "kthread_create_on_node" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0xffd5a395, "default_wake_function" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xde1a6c5d, "skb_queue_purge" },
	{ 0x3684ac1b, "default_llseek" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x34908c14, "print_hex_dump_bytes" },
	{ 0x27e1a049, "printk" },
	{ 0xfb1b7218, "kthread_stop" },
	{ 0x11a13e31, "_kstrtol" },
	{ 0x5bc145de, "hci_alloc_dev" },
	{ 0x520e4027, "debugfs_remove" },
	{ 0x27a53d9a, "skb_push" },
	{ 0x881f8377, "skb_queue_tail" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x1000e51, "schedule" },
	{ 0x9ea9c0d7, "kfree_skb" },
	{ 0x499ab765, "wake_up_process" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{  0xf1338, "__wake_up" },
	{ 0xd2965f6f, "kthread_should_stop" },
	{ 0x78a33819, "add_wait_queue" },
	{ 0x37a0cba, "kfree" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0x8486ed8a, "skb_dequeue" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x6e662ba5, "hci_free_dev" },
	{ 0x1868d40c, "skb_put" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "8899B020900B1B051A764F2");
