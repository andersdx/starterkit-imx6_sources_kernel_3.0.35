#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x297b93fa, "no_llseek" },
	{ 0x1666a6aa, "misc_deregister" },
	{ 0x96576467, "misc_register" },
	{  0xf1338, "__wake_up" },
	{ 0x881f8377, "skb_queue_tail" },
	{ 0x27a53d9a, "skb_push" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0x951cc112, "skb_queue_head" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0x1000e51, "schedule" },
	{ 0x8486ed8a, "skb_dequeue" },
	{ 0x9ea9c0d7, "kfree_skb" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0x3c5ea003, "hci_recv_frame" },
	{ 0xc9bb874, "skb_pull" },
	{ 0x1868d40c, "skb_put" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0xde1a6c5d, "skb_queue_purge" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xb1f56e2d, "nonseekable_open" },
	{ 0x157c02a4, "hci_register_dev" },
	{ 0x5bc145de, "hci_alloc_dev" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x37a0cba, "kfree" },
	{ 0x27e1a049, "printk" },
	{ 0x6e662ba5, "hci_free_dev" },
	{ 0x72c0e777, "hci_unregister_dev" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "7CF06B4F970A55146C8639D");
