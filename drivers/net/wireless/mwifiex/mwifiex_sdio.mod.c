#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x8c550e41, "sdio_writeb" },
	{ 0x487388af, "sdio_readb" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x73db21b7, "mwifiex_cancel_hs" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xf1fcd7a, "sdio_writesb" },
	{ 0x732a3c71, "sdio_enable_func" },
	{ 0x45e250c1, "sdio_claim_irq" },
	{ 0x7c999db8, "netif_carrier_on" },
	{ 0xab4d5062, "down_interruptible" },
	{ 0x93286f5b, "netif_carrier_off" },
	{ 0xf1790230, "queue_work" },
	{ 0xd7d3c8b3, "mwifiex_disable_auto_ds" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x8ace6733, "mwifiex_process_sleep_confirm_resp" },
	{ 0x249b12f0, "sdio_get_host_pm_caps" },
	{ 0x1a902d40, "skb_trim" },
	{ 0x9499564f, "mwifiex_add_card" },
	{ 0x6f5789d3, "dev_alloc_skb" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x27e1a049, "printk" },
	{ 0x9fba3571, "mwifiex_deauthenticate" },
	{ 0xc9bb874, "skb_pull" },
	{ 0xf7cd9af8, "dev_kfree_skb_any" },
	{ 0x77f80f2, "sdio_readsb" },
	{ 0x2a6e891d, "sdio_unregister_driver" },
	{ 0xd597d86e, "sdio_set_host_pm_flags" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x88b7297, "sdio_release_irq" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x82ead7b4, "up" },
	{ 0xeafceb57, "mwifiex_handle_rx_packet" },
	{ 0x2773a281, "dev_warn" },
	{ 0x34b011bc, "mwifiex_remove_card" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xa9674ed7, "mwifiex_enable_hs" },
	{ 0x99bb8806, "memmove" },
	{ 0x69c8fb01, "sdio_register_driver" },
	{ 0x38b3a5a4, "sdio_claim_host" },
	{ 0x1868d40c, "skb_put" },
	{ 0xa9c89c83, "mwifiex_init_shutdown_fw" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xcc7965f0, "sdio_set_block_size" },
	{ 0xd2389047, "sdio_disable_func" },
	{ 0x211ee6cf, "sdio_release_host" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mwifiex";

MODULE_ALIAS("sdio:c*v02DFd9119*");

MODULE_INFO(srcversion, "D52967C9127F4AC62293B0D");
