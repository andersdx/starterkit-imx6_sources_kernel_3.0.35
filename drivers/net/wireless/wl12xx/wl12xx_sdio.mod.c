#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x3ce4ca6f, "disable_irq" },
	{ 0x53dff67d, "wl12xx_debug_level" },
	{ 0xabecf7ad, "__pm_runtime_idle" },
	{ 0x6c06878b, "wl1271_free_hw" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x27bbf221, "disable_irq_nosync" },
	{ 0xf30c2a0a, "wl1271_init_ieee80211" },
	{ 0xf1fcd7a, "sdio_writesb" },
	{ 0x732a3c71, "sdio_enable_func" },
	{ 0x7aacc5d2, "__pm_runtime_resume" },
	{ 0x94864c7a, "wl12xx_get_platform_data" },
	{ 0x249b12f0, "sdio_get_host_pm_caps" },
	{ 0xce2840e7, "irq_set_irq_wake" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x27e1a049, "printk" },
	{ 0xe795ba26, "wl1271_irq" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0x26e7a5e5, "device_init_wakeup" },
	{ 0x77f80f2, "sdio_readsb" },
	{ 0x2a6e891d, "sdio_unregister_driver" },
	{ 0xfa505ba0, "sdio_f0_writeb" },
	{ 0xd597d86e, "sdio_set_host_pm_flags" },
	{ 0x4059792f, "print_hex_dump" },
	{ 0x1d0725c4, "sdio_f0_readb" },
	{ 0x10a78ffb, "pm_wakeup_event" },
	{ 0xfa33bd93, "mmc_power_save_host" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xc7dc58e3, "mmc_power_restore_host" },
	{ 0xc141f3a, "sdio_memcpy_toio" },
	{ 0xfcec0987, "enable_irq" },
	{ 0xb072725, "wl1271_unregister_hw" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xad998077, "complete" },
	{ 0x69c8fb01, "sdio_register_driver" },
	{ 0x43f51885, "sdio_memcpy_fromio" },
	{ 0x38b3a5a4, "sdio_claim_host" },
	{ 0xbaa2b187, "wl1271_alloc_hw" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xcc7965f0, "sdio_set_block_size" },
	{ 0xe4ec633c, "wl1271_register_hw" },
	{ 0xd2389047, "sdio_disable_func" },
	{ 0xf20dabd8, "free_irq" },
	{ 0x211ee6cf, "sdio_release_host" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=wl12xx";

MODULE_ALIAS("sdio:c*v0097d4076*");
