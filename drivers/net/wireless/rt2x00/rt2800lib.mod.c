#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xa1dd72ae, "skb_pad" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x27e1a049, "printk" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x9790b259, "rt2x00mac_conf_tx" },
	{ 0x27a53d9a, "skb_push" },
	{ 0x42f006db, "mutex_lock" },
	{ 0xc9bb874, "skb_pull" },
	{ 0x40d9dc3c, "rt2x00lib_txdone" },
	{ 0xf7cd9af8, "dev_kfree_skb_any" },
	{ 0x9b6d091e, "ieee80211_stop_tx_ba_cb_irqsafe" },
	{ 0xeb060b2a, "rt2x00queue_get_entry" },
	{ 0xc27487dd, "__bug" },
	{ 0x3771b461, "crc_ccitt" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xd2fbfa9a, "rt2x00lib_txdone_noinfo" },
	{ 0xdb101d74, "ieee80211_start_tx_ba_cb_irqsafe" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,mac80211,crc-ccitt";


MODULE_INFO(srcversion, "066CCD790E548F92083F5D6");
