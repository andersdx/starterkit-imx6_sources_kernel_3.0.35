#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xc068440e, "__kfifo_alloc" },
	{ 0xb6718429, "ieee80211_queue_work" },
	{ 0x290ce1be, "__alloc_workqueue_key" },
	{ 0xed0913fb, "led_classdev_register" },
	{ 0x3b502f70, "_raw_spin_lock_bh" },
	{ 0x888e2da, "ieee80211_rts_get" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x3471c32, "ieee80211_beacon_get_tim" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0x4205ad24, "cancel_work_sync" },
	{ 0xf1790230, "queue_work" },
	{ 0x9caafab4, "ieee80211_unregister_hw" },
	{ 0x7513e94e, "ieee80211_channel_to_frequency" },
	{ 0x54f2e7e5, "led_classdev_resume" },
	{ 0x157da3f8, "init_timer_key" },
	{ 0xe1d61c3a, "cancel_delayed_work_sync" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0xbeadd82a, "ieee80211_iterate_active_interfaces_atomic" },
	{ 0x4a37af33, "___dma_single_cpu_to_dev" },
	{ 0x4fb1cc31, "wiphy_rfkill_start_polling" },
	{ 0x7d11c268, "jiffies" },
	{ 0x1a902d40, "skb_trim" },
	{ 0xe8f207b, "ieee80211_stop_queues" },
	{ 0x6a697b04, "ieee80211_stop_queue" },
	{ 0x4570a779, "ieee80211_tx_status" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x42e2b49e, "del_timer_sync" },
	{ 0x6f5789d3, "dev_alloc_skb" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0xdb3877d, "___dma_single_dev_to_cpu" },
	{ 0x21f72eb8, "ieee80211_iterate_active_interfaces" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x2c398f70, "ieee80211_rx" },
	{ 0x27a53d9a, "skb_push" },
	{ 0x42f006db, "mutex_lock" },
	{ 0xa903d1d8, "destroy_workqueue" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0xc9bb874, "skb_pull" },
	{ 0x5f6756d0, "ieee80211_ctstoself_get" },
	{ 0xe2f48b0c, "wiphy_rfkill_stop_polling" },
	{ 0x7cb1cb14, "ieee80211_queue_delayed_work" },
	{ 0xf7cd9af8, "dev_kfree_skb_any" },
	{ 0x82072614, "tasklet_kill" },
	{ 0x3ff62317, "local_bh_disable" },
	{ 0xc73dd955, "_raw_spin_unlock_bh" },
	{ 0x8c9503b5, "wiphy_rfkill_set_hw_state" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0x799aca4, "local_bh_enable" },
	{ 0x90c7564f, "ieee80211_get_buffered_bc" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xfde4e11e, "ieee80211_wake_queue" },
	{ 0xdb760f52, "__kfifo_free" },
	{ 0x9fd34bf1, "ieee80211_get_hdrlen_from_skb" },
	{ 0x5e4b8a, "ieee80211_register_hw" },
	{ 0xba11b12b, "led_classdev_unregister" },
	{ 0x1eb9516e, "round_jiffies_relative" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x243f66ef, "led_classdev_suspend" },
	{ 0xd5b9b782, "request_firmware" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0x99bb8806, "memmove" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x1868d40c, "skb_put" },
	{ 0xdb6568a9, "release_firmware" },
	{ 0x4d134f30, "queue_delayed_work" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mac80211";


MODULE_INFO(srcversion, "559D686C86A33988F6D8B36");
