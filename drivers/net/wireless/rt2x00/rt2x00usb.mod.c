#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0xe2a99689, "usb_kill_urb" },
	{ 0xf1790230, "queue_work" },
	{ 0x96cbed8b, "rt2x00lib_resume" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x157da3f8, "init_timer_key" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x7d11c268, "jiffies" },
	{ 0x6078ef7f, "rt2x00lib_dmastart" },
	{ 0x48b82649, "rt2x00queue_flush_queue" },
	{ 0x27e1a049, "printk" },
	{ 0x765f8d9c, "usb_control_msg" },
	{ 0x5459f8ea, "rt2x00queue_for_each_entry" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x5e586709, "rt2x00lib_remove_dev" },
	{ 0x60bdc989, "usb_submit_urb" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xc7683ccd, "usb_get_dev" },
	{ 0xeb060b2a, "rt2x00queue_get_entry" },
	{ 0x3a2544a8, "rt2x00lib_rxdone" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0x15a3a990, "usb_put_dev" },
	{ 0xc27487dd, "__bug" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0xc228126c, "ieee80211_alloc_hw" },
	{ 0x22e3c97d, "ieee80211_free_hw" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xd2fbfa9a, "rt2x00lib_txdone_noinfo" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x4a3c8a3b, "rt2x00lib_dmadone" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xaf7f7c2b, "usb_free_urb" },
	{ 0xd1d19f90, "rt2x00lib_probe_dev" },
	{ 0x85f42078, "rt2x00lib_suspend" },
	{ 0xa28d894f, "usb_alloc_urb" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00lib,mac80211";


MODULE_INFO(srcversion, "0DE8D6B04A3CAE3EC5468A2");
