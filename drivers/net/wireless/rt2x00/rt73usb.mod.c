#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xb78c61e8, "param_ops_bool" },
	{ 0xe7206e20, "rt2x00usb_resume" },
	{ 0x423d05bb, "rt2x00usb_suspend" },
	{ 0x8aeaf019, "rt2x00usb_disconnect" },
	{ 0xa23de99, "rt2x00mac_get_ringparam" },
	{ 0x4d82af28, "rt2x00mac_get_antenna" },
	{ 0x845f3233, "rt2x00mac_set_antenna" },
	{ 0x1a9af55f, "rt2x00mac_flush" },
	{ 0x83a2655d, "rt2x00mac_rfkill_poll" },
	{ 0x73151ec0, "rt2x00mac_get_stats" },
	{ 0xc5b907e2, "rt2x00mac_sw_scan_complete" },
	{ 0x7e0d6b00, "rt2x00mac_sw_scan_start" },
	{ 0x2c8f3454, "rt2x00mac_set_key" },
	{ 0xfef81a0d, "rt2x00mac_set_tim" },
	{ 0xa83ca2d9, "rt2x00mac_configure_filter" },
	{ 0xa392fde9, "rt2x00mac_bss_info_changed" },
	{ 0xb29dccc8, "rt2x00mac_config" },
	{ 0xf5183fb3, "rt2x00mac_remove_interface" },
	{ 0xe0117242, "rt2x00mac_add_interface" },
	{ 0x507fb485, "rt2x00mac_stop" },
	{ 0x8946e63, "rt2x00mac_start" },
	{ 0x8332d216, "rt2x00mac_tx" },
	{ 0xb3a1f88d, "rt2x00usb_flush_queue" },
	{ 0xbeac70d8, "rt2x00usb_kick_queue" },
	{ 0x94a7a38f, "rt2x00usb_watchdog" },
	{ 0x78439279, "rt2x00usb_clear_entry" },
	{ 0x6c652606, "rt2x00usb_uninitialize" },
	{ 0x416a314e, "rt2x00usb_initialize" },
	{ 0xe5d947e8, "usb_deregister" },
	{ 0x7ae8149a, "usb_register_driver" },
	{ 0xd29b009f, "crc_itu_t_table" },
	{ 0xf5b4a948, "crc_itu_t" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0xf9a482f9, "msleep" },
	{ 0x45ffe081, "rt2x00usb_disable_radio" },
	{ 0x27e1a049, "printk" },
	{ 0xa74f7cb3, "consume_skb" },
	{ 0xa1dd72ae, "skb_pad" },
	{ 0x27a53d9a, "skb_push" },
	{ 0x1a902d40, "skb_trim" },
	{ 0xc9bb874, "skb_pull" },
	{ 0x9d669763, "memcpy" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x665a0fe4, "rt2x00usb_vendor_request" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xc27487dd, "__bug" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x48fbc855, "rt2x00usb_vendor_req_buff_lock" },
	{ 0xe28bf561, "rt2x00usb_regbusy_read" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x9790b259, "rt2x00mac_conf_tx" },
	{ 0x5bc830d1, "rt2x00usb_vendor_request_buff" },
	{ 0x7c92b589, "rt2x00usb_probe" },
	{ 0xff178f6, "__aeabi_idivmod" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=rt2x00usb,rt2x00lib,crc-itu-t";

MODULE_ALIAS("usb:v07B8pB21Bd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07B8pB21Cd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07B8pB21Dd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07B8pB21Ed*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07B8pB21Fd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v14B2p3C10d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v148Fp9021d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0EB0p9021d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v18C5p0002d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1690p0722d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0B05p1723d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0B05p1724d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v050Dp705Ad*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v050Dp905Bd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v050Dp905Cd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1631pC019d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v08DDp0120d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p00D8d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p00D9d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p00F4d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p0116d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p0119d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p0137d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v178Dp02BEd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1371p9022d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1371p9032d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v14B2p3C22d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07AAp002Ed*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07D1p3C03d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07D1p3C04d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07D1p3C06d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07D1p3C07d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v7392p7318d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v7392p7618d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1740p3701d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v15A9p0004d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1044p8008d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1044p800Ad*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1472p0009d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v06F8pE002d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v06F8pE010d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v06F8pE020d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13B1p0020d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13B1p0023d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13B1p0028d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DB0p4600d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DB0p6877d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DB0p6874d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DB0pA861d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DB0pA874d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1B75p7318d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v04BBp093Dd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v148Fp2573d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v148Fp2671d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0812p3101d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v18E8p6196d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v18E8p6229d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v18E8p6238d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v04E8p4471d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1740p7100d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DF6p0024d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DF6p0027d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DF6p002Fd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DF6p90ACd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0DF6p9712d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0769p31F3d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v6933p5001d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0471p200Ad*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v2019pAB01d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v2019pAB50d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v7167p3840d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CDEp001Cd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0586p3415d*dc*dsc*dp*ic*isc*ip*");

MODULE_INFO(srcversion, "A934BD4EDD13F40E6D1C5D3");
