#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x8c550e41, "sdio_writeb" },
	{ 0x487388af, "sdio_readb" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x290ce1be, "__alloc_workqueue_key" },
	{ 0xafccbe4, "lbs_resume" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xf1fcd7a, "sdio_writesb" },
	{ 0x732a3c71, "sdio_enable_func" },
	{ 0x45e250c1, "sdio_claim_irq" },
	{ 0xf1790230, "queue_work" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x9c39e749, "lbs_stop_card" },
	{ 0x6e7bb7d8, "netdev_alert" },
	{ 0xed4cf0b8, "__lbs_cmd" },
	{ 0x7d11c268, "jiffies" },
	{ 0x249b12f0, "sdio_get_host_pm_caps" },
	{ 0xe2d5255a, "strcmp" },
	{ 0x169003d, "lbs_start_card" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x6f5789d3, "dev_alloc_skb" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x27e1a049, "printk" },
	{ 0x42224298, "sscanf" },
	{ 0x712b6abe, "lbs_queue_event" },
	{ 0xa903d1d8, "destroy_workqueue" },
	{ 0xb39f4c2, "lbs_cmd_copyback" },
	{ 0xdbfc0d1c, "lbs_suspend" },
	{ 0x74590879, "flush_workqueue" },
	{ 0x77f80f2, "sdio_readsb" },
	{ 0x2a6e891d, "sdio_unregister_driver" },
	{ 0xe5bd71cb, "lbs_remove_card" },
	{ 0xfa505ba0, "sdio_f0_writeb" },
	{ 0xd597d86e, "sdio_set_host_pm_flags" },
	{ 0xf0f53ba7, "lbs_process_rxed_packet" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x88b7297, "sdio_release_irq" },
	{ 0x58f69637, "lbs_host_to_card_done" },
	{ 0x9d6197c, "sdio_align_size" },
	{ 0xc27487dd, "__bug" },
	{ 0xe416375b, "lbs_add_card" },
	{ 0x1d0725c4, "sdio_f0_readb" },
	{ 0xe9119f4b, "netdev_err" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xa8e66094, "lbs_get_firmware" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x69c8fb01, "sdio_register_driver" },
	{ 0x38b3a5a4, "sdio_claim_host" },
	{ 0x1868d40c, "skb_put" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xcc7965f0, "sdio_set_block_size" },
	{ 0xdb6568a9, "release_firmware" },
	{ 0xd2389047, "sdio_disable_func" },
	{ 0x211ee6cf, "sdio_release_host" },
	{ 0xdeafe881, "lbs_notify_command_response" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libertas";

MODULE_ALIAS("sdio:c*v02DFd9103*");
MODULE_ALIAS("sdio:c*v02DFd9104*");
