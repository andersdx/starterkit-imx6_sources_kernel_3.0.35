#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xcf5613f6, "pci_unregister_driver" },
	{ 0x49e084d2, "__pci_register_driver" },
	{ 0x40a6f522, "__arm_ioremap" },
	{ 0xf2d5c52c, "pci_request_selected_regions" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x8a4f5ef, "alloc_etherdev_mqs" },
	{ 0xbd4c9d20, "pci_set_master" },
	{ 0x94b38ce3, "pci_enable_device_mem" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x2ed501a6, "pci_disable_device" },
	{ 0x5def360, "free_netdev" },
	{ 0x3e3d1adf, "pci_release_selected_regions" },
	{ 0x731f29b9, "pci_select_bars" },
	{ 0x45a55ec8, "__iounmap" },
	{ 0xbafef316, "dev_get_drvdata" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v00008086d00001532sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "B7169D49FD5340E1CA1D99C");
