#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x6d662533, "_find_first_bit_le" },
	{ 0x9f205f91, "kobject_put" },
	{ 0x2b7266bc, "netdev_info" },
	{ 0x8abde603, "pci_bus_read_config_byte" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0xabecf7ad, "__pm_runtime_idle" },
	{ 0xff178f6, "__aeabi_idivmod" },
	{ 0x40cd0666, "mem_map" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0x91eb9b4, "round_jiffies" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xa1dd72ae, "skb_pad" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xf19e9355, "cpu_online_mask" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0xb3c897d6, "napi_complete" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x2ed501a6, "pci_disable_device" },
	{ 0xc7a4fbed, "rtnl_lock" },
	{ 0x7c999db8, "netif_carrier_on" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x8949858b, "schedule_work" },
	{ 0xd3f57a2, "_find_next_bit_le" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0x93286f5b, "netif_carrier_off" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x157da3f8, "init_timer_key" },
	{ 0x7aacc5d2, "__pm_runtime_resume" },
	{ 0x999e8297, "vfree" },
	{ 0x4a37af33, "___dma_single_cpu_to_dev" },
	{ 0xbf351ae7, "pci_bus_write_config_word" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xc8ffbf8c, "__alloc_pages_nodemask" },
	{ 0x44565f25, "sysfs_remove_group" },
	{ 0xb916b6ea, "netif_napi_del" },
	{ 0x7d11c268, "jiffies" },
	{ 0xd1190a9, "kobject_create_and_add" },
	{ 0xbfb36fb6, "__netdev_alloc_skb" },
	{ 0x9b8f11c2, "__pskb_pull_tail" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xbd4c9d20, "pci_set_master" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x42e2b49e, "del_timer_sync" },
	{ 0x5ffd7625, "igb_get_platform_data" },
	{ 0x5f754e5a, "memset" },
	{ 0x7a451ed, "pci_enable_pcie_error_reporting" },
	{ 0xcdd3cd7d, "pci_restore_state" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x27e1a049, "printk" },
	{ 0x34dac548, "sysfs_create_group" },
	{ 0xdb3877d, "___dma_single_dev_to_cpu" },
	{ 0x5def360, "free_netdev" },
	{ 0x328a05f1, "strncpy" },
	{ 0xeb1ab566, "register_netdev" },
	{ 0x35fd3687, "dma_free_coherent" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x610b6d19, "__pci_enable_wake" },
	{ 0x7758eb37, "dev_close" },
	{ 0xa1a662de, "mod_timer" },
	{ 0x981ceb2b, "netif_napi_add" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0xc9bb874, "skb_pull" },
	{ 0xa0b80f88, "__get_page_tail" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0xf7cd9af8, "dev_kfree_skb_any" },
	{ 0x975124e8, "contig_page_data" },
	{ 0x39544d8d, "dma_alloc_coherent" },
	{ 0xb7ba2f6c, "dev_open" },
	{ 0xe523ad75, "synchronize_irq" },
	{ 0xe08ce719, "pci_find_capability" },
	{ 0x731f29b9, "pci_select_bars" },
	{ 0xc6cbbc89, "capable" },
	{ 0x80c1f3c6, "netif_device_attach" },
	{ 0x4fbaffd7, "napi_gro_receive" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x40a9b349, "vzalloc" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xf5a82c0, "netif_device_detach" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0x12a38747, "usleep_range" },
	{ 0x8b42d4f1, "pci_bus_read_config_word" },
	{ 0xf0bb4145, "__napi_schedule" },
	{ 0xd9cd5895, "pci_cleanup_aer_uncorrect_error_status" },
	{ 0xc880aca5, "___dma_page_cpu_to_dev" },
	{ 0x9ea9c0d7, "kfree_skb" },
	{ 0xa9845e0e, "pm_schedule_suspend" },
	{ 0xc3ded3fd, "___dma_page_dev_to_cpu" },
	{ 0x4a8dcb71, "eth_type_trans" },
	{ 0xc27487dd, "__bug" },
	{ 0x287e02c6, "pskb_expand_head" },
	{ 0xe9119f4b, "netdev_err" },
	{ 0xcf5613f6, "pci_unregister_driver" },
	{ 0xcc5005fe, "msleep_interruptible" },
	{ 0xf6ebc03b, "net_ratelimit" },
	{ 0x2a3142da, "pci_set_power_state" },
	{ 0xfd9e1756, "eth_validate_addr" },
	{ 0x1c5e1372, "pci_disable_pcie_error_reporting" },
	{ 0x44da5d0f, "__csum_ipv6_magic" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x801678, "flush_scheduled_work" },
	{ 0x6c105aec, "___pskb_trim" },
	{ 0xf59f197, "param_array_ops" },
	{ 0x5ac1e449, "pci_prepare_to_sleep" },
	{ 0x49e084d2, "__pci_register_driver" },
	{ 0x2288378f, "system_state" },
	{ 0x49c7f3a7, "put_page" },
	{ 0x74c134b9, "__sw_hweight32" },
	{ 0x45a55ec8, "__iounmap" },
	{ 0x2773a281, "dev_warn" },
	{ 0xe4bcdf14, "unregister_netdev" },
	{ 0x40a6f522, "__arm_ioremap" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x22023b56, "__netif_schedule" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0xa74f7cb3, "consume_skb" },
	{ 0x94b38ce3, "pci_enable_device_mem" },
	{ 0x85670f1d, "rtnl_is_locked" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x1868d40c, "skb_put" },
	{ 0x4edcceb5, "pci_wake_from_d3" },
	{ 0x3e3d1adf, "pci_release_selected_regions" },
	{ 0xf2d5c52c, "pci_request_selected_regions" },
	{ 0xa170bbdb, "outer_cache" },
	{ 0x552c4b3b, "skb_copy_bits" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0x6e720ff2, "rtnl_unlock" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0x2d6d2adb, "device_set_wakeup_enable" },
	{ 0xf20dabd8, "free_irq" },
	{ 0x605f8aee, "pci_save_state" },
	{ 0x8a4f5ef, "alloc_etherdev_mqs" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("pci:v00008086d00001533sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001536sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001537sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001538sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001539sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001521sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001522sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001523sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001524sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000150Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000150Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001527sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001510sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001511sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001516sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000438sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000043Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000043Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000440sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010C9sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000150Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001518sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010E6sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010E7sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000150Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00001526sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010E8sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010A7sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010A9sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d000010D6sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "03ACC85E506371AE0163A05");
