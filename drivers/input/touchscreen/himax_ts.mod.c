#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xf7dd1580, "i2c_del_driver" },
	{ 0x2f92c3f1, "i2c_register_driver" },
	{ 0x1ab2e31a, "input_mt_report_pointer_emulation" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xcd77797c, "input_mt_report_slot_state" },
	{ 0xe7e7847d, "input_event" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x26e7a5e5, "device_init_wakeup" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0xdc6e5460, "input_free_device" },
	{ 0xfa2cbdc2, "input_register_device" },
	{ 0x753e6762, "input_mt_init_slots" },
	{ 0x12d5fe8d, "input_set_abs_params" },
	{ 0x7e32a9b3, "input_allocate_device" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x2773a281, "dev_warn" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x37a0cba, "kfree" },
	{ 0xda9537bd, "input_unregister_device" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xf20dabd8, "free_irq" },
	{ 0xce2840e7, "irq_set_irq_wake" },
	{ 0x42f9d2ad, "i2c_master_recv" },
	{ 0xf9a482f9, "msleep" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0xb8d10421, "i2c_master_send" },
	{ 0xbafef316, "dev_get_drvdata" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("i2c:hx8520-c");
MODULE_ALIAS("i2c:hx8526-a");
