#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x9d46dec0, "spi_bus_type" },
	{ 0x8d14e8c9, "driver_unregister" },
	{ 0xb809ccfa, "spi_register_driver" },
	{ 0xce2840e7, "irq_set_irq_wake" },
	{ 0xda9537bd, "input_unregister_device" },
	{ 0xdc6e5460, "input_free_device" },
	{ 0x29a2ca89, "regulator_put" },
	{ 0xf20dabd8, "free_irq" },
	{ 0xe6625ffe, "hwmon_device_unregister" },
	{ 0x26e7a5e5, "device_init_wakeup" },
	{ 0xfa2cbdc2, "input_register_device" },
	{ 0x44565f25, "sysfs_remove_group" },
	{ 0xad061e41, "hwmon_device_register" },
	{ 0x34dac548, "sysfs_create_group" },
	{ 0x2773a281, "dev_warn" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0xd63dd0ae, "regulator_get" },
	{ 0x12d5fe8d, "input_set_abs_params" },
	{ 0x701d0ebd, "snprintf" },
	{ 0xfe990052, "gpio_free" },
	{ 0x65d6d0f0, "gpio_direction_input" },
	{ 0x47229b5c, "gpio_request" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x7e32a9b3, "input_allocate_device" },
	{ 0x2ec0bdb3, "spi_setup" },
	{ 0x9e7d6bd0, "__udelay" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xe7e7847d, "input_event" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0xc27487dd, "__bug" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0xf9a482f9, "msleep" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x6c8d5ae8, "__gpio_get_value" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x37a0cba, "kfree" },
	{ 0xb0516606, "spi_sync" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x2ea67d9b, "regulator_enable" },
	{ 0x996bdb64, "_kstrtoul" },
	{ 0xe8ab84fc, "regulator_disable" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x3ce4ca6f, "disable_irq" },
	{  0xf1338, "__wake_up" },
	{ 0xa170bbdb, "outer_cache" },
	{ 0xfcec0987, "enable_irq" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

