#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xeb8c043f, "drm_release" },
	{ 0x22e5573d, "drm_core_reclaim_buffers" },
	{ 0x4b1ecd86, "drm_mmap" },
	{ 0x27e1a049, "printk" },
	{ 0xe3d200bd, "drm_platform_exit" },
	{ 0xd8381108, "noop_llseek" },
	{ 0x69b41207, "platform_device_unregister" },
	{ 0xd869c633, "drm_platform_init" },
	{ 0xb00a5a43, "drm_ioctl" },
	{ 0xb09d9ebe, "platform_device_register_resndata" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xf9c4701d, "drm_poll" },
	{ 0xc388b85, "drm_fasync" },
	{ 0xd8e22f87, "drm_open" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=drm";

