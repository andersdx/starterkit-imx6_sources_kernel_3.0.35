#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x9d33cf71, "device_remove_file" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xff178f6, "__aeabi_idivmod" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x7c999db8, "netif_carrier_on" },
	{ 0x4b3f1925, "skb_clone" },
	{ 0x20000329, "simple_strtoul" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x11f7ed4c, "hex_to_bin" },
	{ 0x8949858b, "schedule_work" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0x93286f5b, "netif_carrier_off" },
	{ 0xb78c61e8, "param_ops_bool" },
	{ 0x96bc3be8, "usb_gadget_unregister_driver" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x9ab271af, "skb_realloc_headroom" },
	{ 0x7d11c268, "jiffies" },
	{ 0x1a902d40, "skb_trim" },
	{ 0x66c53be4, "netif_rx" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x11089ac7, "_ctype" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x27e1a049, "printk" },
	{ 0x1757ff46, "ethtool_op_get_link" },
	{ 0x5def360, "free_netdev" },
	{ 0xeb1ab566, "register_netdev" },
	{ 0x73e20c1c, "strlcpy" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x27a53d9a, "skb_push" },
	{ 0xa34f1ef5, "crc32_le" },
	{ 0x89ff43f6, "init_uts_ns" },
	{ 0xc9bb874, "skb_pull" },
	{ 0xf7cd9af8, "dev_kfree_skb_any" },
	{ 0xb0582f98, "device_create_file" },
	{ 0x24e1307e, "flush_work_sync" },
	{ 0x881f8377, "skb_queue_tail" },
	{ 0x1d2b1d69, "skb_copy_expand" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x9f984513, "strrchr" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xab52c1d9, "__alloc_skb" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0x4a8dcb71, "eth_type_trans" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xfd9e1756, "eth_validate_addr" },
	{ 0x8d66a3a, "warn_slowpath_fmt" },
	{ 0x1c8e667d, "usb_gadget_probe_driver" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x8486ed8a, "skb_dequeue" },
	{ 0x2773a281, "dev_warn" },
	{ 0xe4bcdf14, "unregister_netdev" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x22023b56, "__netif_schedule" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0x8436f8e3, "param_ops_ushort" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x1868d40c, "skb_put" },
	{ 0xc35ba859, "eth_mac_addr" },
	{ 0xc3fe87c8, "param_ops_uint" },
	{ 0x552c4b3b, "skb_copy_bits" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xcbf41ec1, "dev_get_stats" },
	{ 0x8a4f5ef, "alloc_etherdev_mqs" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

