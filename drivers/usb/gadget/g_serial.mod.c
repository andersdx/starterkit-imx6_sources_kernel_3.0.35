#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x9d33cf71, "device_remove_file" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x55684269, "alloc_tty_driver" },
	{ 0x20000329, "simple_strtoul" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x46069e4c, "tty_register_driver" },
	{ 0xb78c61e8, "param_ops_bool" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x51e70945, "put_tty_driver" },
	{ 0x96bc3be8, "usb_gadget_unregister_driver" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xee4c8a93, "tty_set_operations" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x11089ac7, "_ctype" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0xfd2936bf, "tty_insert_flip_string_fixed_flag" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0x89ff43f6, "init_uts_ns" },
	{ 0x7f1b1929, "tty_register_device" },
	{ 0x82072614, "tasklet_kill" },
	{ 0xb0582f98, "device_create_file" },
	{ 0xe24ecd7e, "tty_unregister_device" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x9f984513, "strrchr" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x67b27ec1, "tty_std_termios" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x1000e51, "schedule" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0x6889622d, "tty_unregister_driver" },
	{ 0x81e7fd4f, "tty_hangup" },
	{  0xf1338, "__wake_up" },
	{ 0x8d66a3a, "warn_slowpath_fmt" },
	{ 0x1c8e667d, "usb_gadget_probe_driver" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0x2773a281, "dev_warn" },
	{ 0x9dee60a0, "tty_flip_buffer_push" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0x8436f8e3, "param_ops_ushort" },
	{ 0xeded33f0, "tty_wakeup" },
	{ 0xc3fe87c8, "param_ops_uint" },
	{ 0xbafef316, "dev_get_drvdata" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

