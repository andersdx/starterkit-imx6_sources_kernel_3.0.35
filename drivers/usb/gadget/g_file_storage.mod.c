#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xe861b005, "d_path" },
	{ 0x9d33cf71, "device_remove_file" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xd5b037e1, "kref_put" },
	{ 0xa0db100e, "complete_and_exit" },
	{ 0x2d80f5d1, "up_read" },
	{ 0xa4f5f5f, "dequeue_signal" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x20000329, "simple_strtoul" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0xb08a7674, "filp_close" },
	{ 0xb78c61e8, "param_ops_bool" },
	{ 0x5b4916c8, "vfs_fsync" },
	{ 0x96bc3be8, "usb_gadget_unregister_driver" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0x34e55394, "kthread_create_on_node" },
	{ 0xfc1eab1f, "down_read" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0x2f4ea1ac, "wait_for_completion" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0x504861b2, "vfs_read" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xd13b5482, "device_register" },
	{ 0x11089ac7, "_ctype" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x89ff43f6, "init_uts_ns" },
	{ 0xab728736, "up_write" },
	{ 0xd77ff6df, "down_write" },
	{ 0xfce76a50, "fput" },
	{ 0xd79b5a02, "allow_signal" },
	{ 0xb0582f98, "device_create_file" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x9f984513, "strrchr" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x4b0ed5c, "put_device" },
	{ 0x1000e51, "schedule" },
	{ 0x35c2ba9e, "refrigerator" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0x499ab765, "wake_up_process" },
	{ 0xcc5005fe, "msleep_interruptible" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0x1c8e667d, "usb_gadget_probe_driver" },
	{ 0x83800bfa, "kref_init" },
	{ 0x37a0cba, "kfree" },
	{ 0xa46f2f1b, "kstrtouint" },
	{ 0x9d669763, "memcpy" },
	{ 0xf59f197, "param_array_ops" },
	{ 0x7e0587ed, "send_sig_info" },
	{ 0x9775cdc, "kref_get" },
	{ 0x2f70ee96, "invalidate_mapping_pages" },
	{ 0x2773a281, "dev_warn" },
	{ 0x538fe0d9, "device_unregister" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xb742fd7, "simple_strtol" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xad998077, "complete" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x99bb8806, "memmove" },
	{ 0x117402d5, "dev_set_name" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0xc3fe87c8, "param_ops_uint" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xc0e38433, "__init_rwsem" },
	{ 0x589a8e9d, "vfs_write" },
	{ 0x1703fb2c, "filp_open" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

