#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x9d33cf71, "device_remove_file" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x4c63c1a1, "snd_pcm_lib_write" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x20000329, "simple_strtoul" },
	{ 0x8949858b, "schedule_work" },
	{ 0xb08a7674, "filp_close" },
	{ 0x96bc3be8, "usb_gadget_unregister_driver" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x11089ac7, "_ctype" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x89ff43f6, "init_uts_ns" },
	{ 0xb0582f98, "device_create_file" },
	{ 0x601a8d1d, "_dev_info" },
	{ 0x9f984513, "strrchr" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xe551272c, "_raw_spin_lock_irq" },
	{ 0xcc5005fe, "msleep_interruptible" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0x8d66a3a, "warn_slowpath_fmt" },
	{ 0x1c8e667d, "usb_gadget_probe_driver" },
	{ 0x4cda566, "snd_interval_refine" },
	{ 0x37a0cba, "kfree" },
	{ 0x283dfe3, "_snd_pcm_hw_params_any" },
	{ 0x9d669763, "memcpy" },
	{ 0x2773a281, "dev_warn" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xab5ac2ce, "snd_pcm_kernel_ioctl" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x8436f8e3, "param_ops_ushort" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0x1703fb2c, "filp_open" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

