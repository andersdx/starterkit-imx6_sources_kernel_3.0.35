#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xb96f7911, "hid_unregister_driver" },
	{ 0x415cbc93, "__hid_register_driver" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x4613f78e, "hid_connect" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x37a0cba, "kfree" },
	{ 0xf9c03721, "hid_disconnect" },
	{ 0x1ab2e31a, "input_mt_report_pointer_emulation" },
	{ 0xcd77797c, "input_mt_report_slot_state" },
	{ 0xe7e7847d, "input_event" },
	{ 0x753e6762, "input_mt_init_slots" },
	{ 0x4f107ae5, "input_set_capability" },
	{ 0x12d5fe8d, "input_set_abs_params" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x31cc372b, "usbhid_submit_report" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xbafef316, "dev_get_drvdata" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("hid:b0003v00000596p00000500");
MODULE_ALIAS("hid:b0003v00000596p00000502");
MODULE_ALIAS("hid:b0003v00000596p00000506");
MODULE_ALIAS("hid:b0003v00002101p00001011");
MODULE_ALIAS("hid:b0003v00002453p00000100");
MODULE_ALIAS("hid:b0003v00002087p00000A01");
MODULE_ALIAS("hid:b0003v00002087p00000A02");
MODULE_ALIAS("hid:b0003v00002087p00000B03");
MODULE_ALIAS("hid:b0003v00002087p00000F01");
MODULE_ALIAS("hid:b0003v00002247p00000001");
MODULE_ALIAS("hid:b0003v00001FF7p00000013");
MODULE_ALIAS("hid:b0003v000004B4p0000C001");
MODULE_ALIAS("hid:b0003v00000EEFp0000480D");
MODULE_ALIAS("hid:b0003v00000EEFp0000480E");
MODULE_ALIAS("hid:b0003v00000EEFp0000720C");
MODULE_ALIAS("hid:b0003v00000EEFp0000726B");
MODULE_ALIAS("hid:b0003v00000EEFp000072A1");
MODULE_ALIAS("hid:b0003v00000EEFp000072FA");
MODULE_ALIAS("hid:b0003v00000EEFp00007302");
MODULE_ALIAS("hid:b0003v00000EEFp0000A001");
MODULE_ALIAS("hid:b0003v000004E7p00000022");
MODULE_ALIAS("hid:b0003v00000DFCp00000003");
MODULE_ALIAS("hid:b0003v00001AADp0000000F");
MODULE_ALIAS("hid:b0003v0000222Ap00000001");
MODULE_ALIAS("hid:b0003v00006615p00000070");
MODULE_ALIAS("hid:b0003v00001FD2p00000064");
MODULE_ALIAS("hid:b0003v0000202Ep00000006");
MODULE_ALIAS("hid:b0003v0000202Ep00000007");
MODULE_ALIAS("hid:b0003v00000486p00000185");
MODULE_ALIAS("hid:b0003v00000486p00000186");
MODULE_ALIAS("hid:b0003v0000062Ap00007100");
MODULE_ALIAS("hid:b0003v000014E1p00003500");
MODULE_ALIAS("hid:b0003v000020B3p00000A18");
MODULE_ALIAS("hid:b0003v00002087p00000703");
MODULE_ALIAS("hid:b0003v00001F87p00000002");
MODULE_ALIAS("hid:b0003v00000483p00003261");
MODULE_ALIAS("hid:b0003v00001403p00005001");
MODULE_ALIAS("hid:b0003v00001E5Ep00000313");
MODULE_ALIAS("hid:b0003v0000227Dp00000709");
MODULE_ALIAS("hid:b0003v0000227Dp00000A19");
