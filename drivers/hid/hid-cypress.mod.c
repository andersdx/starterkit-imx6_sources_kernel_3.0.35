#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xb96f7911, "hid_unregister_driver" },
	{ 0x415cbc93, "__hid_register_driver" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x4613f78e, "hid_connect" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xe7e7847d, "input_event" },
	{ 0xbafef316, "dev_get_drvdata" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("hid:b0003v000004B4p0000DE61");
MODULE_ALIAS("hid:b0003v000004B4p0000DE64");
MODULE_ALIAS("hid:b0003v000004B4p0000BCA1");
MODULE_ALIAS("hid:b0003v000004B4p00000001");
