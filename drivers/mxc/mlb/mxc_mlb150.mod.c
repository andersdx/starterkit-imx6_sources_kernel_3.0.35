#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x7427605f, "platform_driver_unregister" },
	{ 0xf75cb01e, "platform_driver_register" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x7b6646bb, "_raw_read_lock" },
	{ 0x546ae5c3, "iram_free" },
	{  0xf1338, "__wake_up" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x366ceba4, "clk_get" },
	{ 0x2ea67d9b, "regulator_enable" },
	{ 0x5272212c, "regulator_set_voltage" },
	{ 0xd63dd0ae, "regulator_get" },
	{ 0x40a6f522, "__arm_ioremap" },
	{ 0xd6b8e852, "request_threaded_irq" },
	{ 0x89b4523d, "platform_get_resource" },
	{ 0xdbd8e713, "device_create" },
	{ 0xd44618e4, "__class_create" },
	{ 0xb63f8646, "cdev_add" },
	{ 0x61f4d58, "cdev_init" },
	{ 0x29537c9e, "alloc_chrdev_region" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xfbc74f64, "__copy_from_user" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0x1000e51, "schedule" },
	{ 0x572d0104, "_raw_write_unlock_irqrestore" },
	{ 0x2f3857e2, "_raw_write_lock_irqsave" },
	{ 0xd41fe818, "_raw_read_unlock_irqrestore" },
	{ 0x2e7be112, "_raw_read_lock_irqsave" },
	{ 0x27e1a049, "printk" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x29338e18, "iram_alloc" },
	{ 0x37a0cba, "kfree" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0x79be06a0, "class_destroy" },
	{ 0x22cb404e, "device_destroy" },
	{ 0xf20dabd8, "free_irq" },
	{ 0x45a55ec8, "__iounmap" },
	{ 0x74b1617f, "gpio_mlb_inactive" },
	{ 0x29a2ca89, "regulator_put" },
	{ 0xe8ab84fc, "regulator_disable" },
	{ 0x2e1ca751, "clk_put" },
	{ 0x4dba43c3, "clk_disable" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xc2b1d97, "clk_enable" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0x8ddab831, "_raw_spin_unlock_irqrestore" },
	{ 0x1a9b678e, "_raw_spin_lock_irqsave" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

