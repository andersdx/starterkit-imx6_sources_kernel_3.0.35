#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0xf9a482f9, "msleep" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xa5adb65, "i2c_transfer" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x42f006db, "mutex_lock" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "7293FD8A130A8CDDCC173AD");
