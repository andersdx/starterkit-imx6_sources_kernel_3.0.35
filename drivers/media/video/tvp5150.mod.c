#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0xd39b0def, "v4l2_subdev_querymenu" },
	{ 0xd067c543, "v4l2_subdev_try_ext_ctrls" },
	{ 0xd5b599c8, "v4l2_subdev_s_ext_ctrls" },
	{ 0x5ff117d1, "v4l2_subdev_g_ext_ctrls" },
	{ 0x3a81b747, "v4l2_subdev_s_ctrl" },
	{ 0x124a9f77, "v4l2_subdev_g_ctrl" },
	{ 0x4a5763f0, "v4l2_subdev_queryctrl" },
	{ 0xf7dd1580, "i2c_del_driver" },
	{ 0x2f92c3f1, "i2c_register_driver" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x7c73a61d, "v4l2_int_device_register" },
	{ 0x10dd3ea0, "v4l2_ctrl_handler_setup" },
	{ 0xb965efc0, "v4l2_ctrl_new_std" },
	{ 0x5fbc13e6, "v4l2_ctrl_handler_init" },
	{ 0xbc0cec93, "v4l2_i2c_subdev_init" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x8602fd3d, "v4l2_chip_ident_i2c_client" },
	{ 0xff178f6, "__aeabi_idivmod" },
	{ 0x37a0cba, "kfree" },
	{ 0xa6c65ec3, "v4l2_ctrl_handler_free" },
	{ 0xc8176bce, "v4l2_device_unregister_subdev" },
	{ 0xba42271a, "v4l2_int_device_unregister" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0x42f9d2ad, "i2c_master_recv" },
	{ 0xf9a482f9, "msleep" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xb8d10421, "i2c_master_send" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x27e1a049, "printk" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("i2c:tvp5150");
