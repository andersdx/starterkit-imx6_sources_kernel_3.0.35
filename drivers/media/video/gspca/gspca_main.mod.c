#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xf9a482f9, "msleep" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xc87c1f84, "ktime_get" },
	{ 0xe2a99689, "usb_kill_urb" },
	{ 0x45f4720c, "__video_register_device" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x999e8297, "vfree" },
	{ 0xfc6e8775, "__init_waitqueue_head" },
	{ 0xdd0a2ba2, "strlcat" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x3db1b80c, "mutex_lock_interruptible" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x1c78979, "video_unregister_device" },
	{ 0x4cb7f01b, "usb_set_interface" },
	{ 0x328a05f1, "strncpy" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x9c6b9bae, "usb_free_coherent" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0x97d0a4a7, "vm_insert_page" },
	{ 0xcec6f16, "module_put" },
	{ 0x60bdc989, "usb_submit_urb" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x7e1144a5, "video_devdata" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0xfa2cbdc2, "input_register_device" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0xc180246, "usb_clear_halt" },
	{ 0xdc6e5460, "input_free_device" },
	{ 0xa0b04675, "vmalloc_32" },
	{ 0x341dbfa3, "__per_cpu_offset" },
	{  0xf1338, "__wake_up" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0xda9537bd, "input_unregister_device" },
	{ 0x69ff5332, "prepare_to_wait" },
	{ 0xbc3d21af, "finish_wait" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0xa56e4663, "usb_ifnum_to_if" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x9e24479f, "vmalloc_to_page" },
	{ 0x24529a9a, "usb_alloc_coherent" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xaf7f7c2b, "usb_free_urb" },
	{ 0x26c7877f, "video_ioctl2" },
	{ 0xa28d894f, "usb_alloc_urb" },
	{ 0xdf4c8767, "ns_to_timeval" },
	{ 0x7e32a9b3, "input_allocate_device" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

