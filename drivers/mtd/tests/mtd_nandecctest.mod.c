#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x7d11c268, "jiffies" },
	{ 0xb89af9bf, "srandom32" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x4059792f, "print_hex_dump" },
	{ 0x27e1a049, "printk" },
	{ 0x71c90087, "memcmp" },
	{ 0xb4b94377, "__nand_correct_data" },
	{ 0x3132ee65, "__nand_calculate_ecc" },
	{ 0xb86e4ab9, "random32" },
	{ 0x9d669763, "memcpy" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xc27487dd, "__bug" },
	{ 0xf7802486, "__aeabi_uidivmod" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

