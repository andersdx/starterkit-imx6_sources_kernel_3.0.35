#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x7427605f, "platform_driver_unregister" },
	{ 0xf75cb01e, "platform_driver_register" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x6cc0ee06, "dev_err" },
	{ 0xaaae1e60, "dev_set_drvdata" },
	{ 0x643d4a69, "backlight_device_register" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xbc71fc56, "pwm_request" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x6682dad6, "pwm_enable" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xe2d5255a, "strcmp" },
	{ 0x37a0cba, "kfree" },
	{ 0x2cf7212f, "pwm_free" },
	{ 0x5ffbc848, "backlight_device_unregister" },
	{ 0x2357faa2, "pwm_disable" },
	{ 0xa896fdf1, "pwm_config" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0x42f006db, "mutex_lock" },
	{ 0xbafef316, "dev_get_drvdata" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

