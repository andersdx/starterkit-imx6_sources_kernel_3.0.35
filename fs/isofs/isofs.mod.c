#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x843e12ca, "kmem_cache_destroy" },
	{ 0xfeb23e28, "iget_failed" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xfbf9cb03, "sb_min_blocksize" },
	{ 0x77ecac9f, "zlib_inflateEnd" },
	{ 0x39de3ea, "__bread" },
	{ 0x7d59db63, "unload_nls" },
	{ 0x4c255938, "save_mount_options" },
	{ 0x3128476f, "generic_file_llseek" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0x97255bdf, "strlen" },
	{ 0xaea8bf3a, "page_address" },
	{ 0xf8fe502b, "iget5_locked" },
	{ 0x42cc9a7, "grab_cache_page_nowait" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0xacf4d843, "match_strdup" },
	{ 0x8d72bd4f, "ll_rw_block" },
	{ 0xcae232b, "utf16s_to_utf8s" },
	{ 0x44e9a829, "match_token" },
	{ 0x2e724c25, "block_read_full_page" },
	{ 0x4e830a3e, "strnicmp" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0xde4ccf85, "mount_bdev" },
	{ 0x85df9b6c, "strsep" },
	{ 0xbf15e368, "page_symlink_inode_operations" },
	{ 0x2703cf54, "generic_read_dir" },
	{ 0x999e8297, "vfree" },
	{ 0x3c2c5af5, "sprintf" },
	{ 0xc8ffbf8c, "__alloc_pages_nodemask" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x11089ac7, "_ctype" },
	{ 0xd627480b, "strncat" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x48933963, "d_obtain_alias" },
	{ 0x5a9a8a50, "d_alloc_root" },
	{ 0xf27fb6cb, "kunmap" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0xce5ac24f, "zlib_inflate_workspacesize" },
	{ 0x84b183ae, "strncmp" },
	{ 0x88ecd84f, "kmem_cache_free" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x89019f70, "__wait_on_buffer" },
	{ 0x28792d8b, "generic_ro_fops" },
	{ 0x4e3567f7, "match_int" },
	{ 0xb9a8be5e, "unlock_page" },
	{ 0x4df1e04b, "__brelse" },
	{ 0x975124e8, "contig_page_data" },
	{ 0x881039d0, "zlib_inflate" },
	{ 0x33fb5fac, "inode_init_once" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0x1098194b, "__free_pages" },
	{ 0xd4d3fc75, "kmap" },
	{ 0x93fca811, "__get_free_pages" },
	{ 0xb287b28d, "load_nls" },
	{ 0x6aa9a632, "unlock_new_inode" },
	{ 0x14e96152, "kill_block_super" },
	{ 0xc27487dd, "__bug" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0xabd3e8ae, "generic_show_options" },
	{ 0x6e12c87d, "kmem_cache_create" },
	{ 0xf0ed772d, "register_filesystem" },
	{ 0x4211c3c1, "zlib_inflateInit2" },
	{ 0x4302d0eb, "free_pages" },
	{ 0x538b6fd6, "iput" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0x50f5e532, "call_rcu_sched" },
	{ 0xc5e196d4, "load_nls_default" },
	{ 0xf76d94c6, "d_splice_alias" },
	{ 0x37fbe687, "sb_set_blocksize" },
	{ 0x49c7f3a7, "put_page" },
	{ 0x6a6b6315, "ioctl_by_bdev" },
	{ 0x96c514e0, "unregister_filesystem" },
	{ 0xdacd1f60, "init_special_inode" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0xa76906e1, "__getblk" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x12ade733, "flush_dcache_page" },
	{ 0x3a34b5d8, "generic_block_bmap" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

