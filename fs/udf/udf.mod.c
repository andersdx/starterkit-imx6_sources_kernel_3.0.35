#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x58ff0ade, "__kmap_atomic" },
	{ 0x843e12ca, "kmem_cache_destroy" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xc6a1c2a, "mark_buffer_dirty_inode" },
	{ 0x2d80f5d1, "up_read" },
	{ 0x39de3ea, "__bread" },
	{ 0x7d59db63, "unload_nls" },
	{ 0x5c0ad473, "make_bad_inode" },
	{ 0x3128476f, "generic_file_llseek" },
	{ 0xab21d454, "__mark_inode_dirty" },
	{ 0x67c2fa54, "__copy_to_user" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xf5b4a948, "crc_itu_t" },
	{ 0xb8869f3d, "simple_write_end" },
	{ 0x815b5dd4, "match_octal" },
	{ 0x67053080, "current_kernel_time" },
	{ 0xbda583dd, "block_write_begin" },
	{ 0xda08612f, "malloc_sizes" },
	{ 0x7d11668c, "seq_puts" },
	{ 0xedf3ccbe, "is_bad_inode" },
	{ 0x2459228b, "generic_file_open" },
	{ 0x7b6646bb, "_raw_read_lock" },
	{ 0x906a12cd, "__lock_page" },
	{ 0x8d72bd4f, "ll_rw_block" },
	{ 0x330eb452, "__lock_buffer" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0xb4d6e54d, "generic_file_aio_read" },
	{ 0xd3f57a2, "_find_next_bit_le" },
	{ 0x7acbd3bd, "seq_printf" },
	{ 0x353e3fa5, "__get_user_4" },
	{ 0x44e9a829, "match_token" },
	{ 0x2e724c25, "block_read_full_page" },
	{ 0x132e59fd, "end_writeback" },
	{ 0xfe4b8084, "mutex_unlock" },
	{ 0xde4ccf85, "mount_bdev" },
	{ 0x85df9b6c, "strsep" },
	{ 0x2703cf54, "generic_read_dir" },
	{ 0x999e8297, "vfree" },
	{ 0xb09c0ede, "unlock_buffer" },
	{ 0x9ec06b0b, "generic_file_aio_write" },
	{ 0x934bdb26, "truncate_setsize" },
	{ 0xfc1eab1f, "down_read" },
	{ 0xe2d5255a, "strcmp" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0xff295a48, "__insert_inode_hash" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x16e66c8e, "simple_write_begin" },
	{ 0xe5fac0a9, "current_fs_time" },
	{ 0xd968a2d9, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x48933963, "d_obtain_alias" },
	{ 0x71c90087, "memcmp" },
	{ 0x281b8d83, "find_or_create_page" },
	{ 0x5a9a8a50, "d_alloc_root" },
	{ 0xf27fb6cb, "kunmap" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x328a05f1, "strncpy" },
	{ 0x84b183ae, "strncmp" },
	{ 0x88ecd84f, "kmem_cache_free" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x42f006db, "mutex_lock" },
	{ 0x89019f70, "__wait_on_buffer" },
	{ 0x1ea06663, "_raw_write_lock" },
	{ 0xed93f29e, "__kunmap_atomic" },
	{ 0x92ca9f13, "setattr_copy" },
	{ 0x601bfa13, "sync_dirty_buffer" },
	{ 0xd2ea24e3, "truncate_pagecache" },
	{ 0x4e3567f7, "match_int" },
	{ 0xb9a8be5e, "unlock_page" },
	{ 0xab728736, "up_write" },
	{ 0xd77ff6df, "down_write" },
	{ 0x4df1e04b, "__brelse" },
	{ 0xfe5d4bb2, "sys_tz" },
	{ 0x33fb5fac, "inode_init_once" },
	{ 0xfef7af85, "page_follow_link_light" },
	{ 0xc6cbbc89, "capable" },
	{ 0xfc245844, "invalidate_inode_buffers" },
	{ 0x6f7a9ca2, "file_permission" },
	{ 0x40a9b349, "vzalloc" },
	{ 0xd4ed596e, "kmem_cache_alloc" },
	{ 0xbc10dd97, "__put_user_4" },
	{ 0x886431d1, "generic_file_mmap" },
	{ 0xd4d3fc75, "kmap" },
	{ 0xacdd1659, "block_write_full_page" },
	{ 0xb287b28d, "load_nls" },
	{ 0x33fce783, "generic_write_end" },
	{ 0x18a41b88, "do_sync_read" },
	{ 0x6aa9a632, "unlock_new_inode" },
	{ 0x14e96152, "kill_block_super" },
	{ 0xc27487dd, "__bug" },
	{ 0x22978722, "inode_change_ok" },
	{ 0xc4097c34, "_raw_spin_lock" },
	{ 0x6e12c87d, "kmem_cache_create" },
	{ 0xf0ed772d, "register_filesystem" },
	{ 0x57a6504e, "vsnprintf" },
	{ 0x538b6fd6, "iput" },
	{ 0xa4415a15, "generic_file_fsync" },
	{ 0x37a0cba, "kfree" },
	{ 0x4db4002a, "do_sync_write" },
	{ 0x7e9712f, "ihold" },
	{ 0x9d669763, "memcpy" },
	{ 0x50f5e532, "call_rcu_sched" },
	{ 0xc5e196d4, "load_nls_default" },
	{ 0xf76d94c6, "d_splice_alias" },
	{ 0xa87eae39, "block_truncate_page" },
	{ 0x37fbe687, "sb_set_blocksize" },
	{ 0x2be09365, "generic_readlink" },
	{ 0x49c7f3a7, "put_page" },
	{ 0x4cbbd171, "__bitmap_weight" },
	{ 0x750cc54a, "mark_buffer_dirty" },
	{ 0x6a6b6315, "ioctl_by_bdev" },
	{ 0x96c514e0, "unregister_filesystem" },
	{ 0xdacd1f60, "init_special_inode" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x9db9e69f, "new_inode" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0x99bb8806, "memmove" },
	{ 0x81e4b32c, "generic_file_splice_read" },
	{ 0xa76906e1, "__getblk" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0xec310353, "page_put_link" },
	{ 0x17facdb2, "d_instantiate" },
	{ 0xc0e38433, "__init_rwsem" },
	{ 0x12ade733, "flush_dcache_page" },
	{ 0x3a34b5d8, "generic_block_bmap" },
	{ 0x2479d325, "iget_locked" },
	{ 0x4dec6038, "memscan" },
	{ 0x58027df1, "inode_init_owner" },
	{ 0xe3b583de, "truncate_inode_pages" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=crc-itu-t";

