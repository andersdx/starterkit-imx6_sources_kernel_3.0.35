#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x19931c24, "module_layout" },
	{ 0x90bbcece, "crypto_unregister_template" },
	{ 0x20fb6f84, "crypto_register_template" },
	{ 0x5f013f30, "shash_register_instance" },
	{ 0xdb52a887, "crypto_mod_put" },
	{ 0x88237a0b, "shash_free_instance" },
	{ 0x1dfef0d, "crypto_init_shash_spawn" },
	{ 0x9f951b54, "crypto_alloc_instance2" },
	{ 0x60c324a5, "shash_attr_alg" },
	{ 0xd16712f3, "crypto_check_attr_type" },
	{ 0x1248d197, "crypto_spawn_tfm2" },
	{ 0xc4e5c93c, "crypto_destroy_tfm" },
	{ 0x716e3e39, "crypto_shash_final" },
	{ 0x637e95ea, "crypto_shash_finup" },
	{ 0xfa2a45e, "__memzero" },
	{ 0x9d669763, "memcpy" },
	{ 0xbc0c646e, "crypto_shash_digest" },
	{ 0x5c5c2154, "crypto_shash_update" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

